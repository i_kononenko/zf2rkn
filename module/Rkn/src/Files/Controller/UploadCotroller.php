<?php
namespace Files\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;

class UploadController extends AbstractActionController {

    public function uploadAction() {
        return new ViewModel(array(
            'form' => $this->ConstructForm(),
                )
        );
    }

    public function ConstructForm() {
        $name = new Element('name');
        $name->setLabel('Your name');
        $name->setAttributes(array(
            'type' => 'text'
        ));

        $email = new Element\Email('email');
        $email->setLabel('Your email address');

        $subject = new Element('subject');
        $subject->setLabel('Subject');
        $subject->setAttributes(array(
            'type' => 'text'
        ));

        $message = new Element\Textarea('message');
        $message->setLabel('Message');

        $send = new Element('send');
        $send->setValue('Submit');
        $send->setAttributes(array(
            'type' => 'submit'
        ));


        $form = new Form('contact');
        $form->add($name);
        $form->add($email);
        $form->add($subject);
        $form->add($message);
        $form->add($send);

        $nameInput = new Input('name');

        $inputFilter = new InputFilter();

        $form->setInputFilter($inputFilter);
        return $form;
    }

}
