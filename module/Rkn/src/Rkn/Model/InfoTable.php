<?php
namespace Rkn\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class InfoTable {
    
    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway){
        $this->tableGateway = $tableGateway;
    }
    
    public function setUseFiles($id){
        if (isset($id)){
             $data = array(                
                'value' => $id,
                'dead_time'  => time(),                
            );           
            $this->tableGateway->update($data, array("name"=>"id_send_files"));            
        }
        else{            
            throw new Exception("Не выбраны файлы");
        }
    }

}
