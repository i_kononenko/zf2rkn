<?php
namespace Rkn\Model;

class Info {
    
    public $id;
    public $name;
    public $value;
    public $dead_time;
    
    public function exchangeArray($data) {
        $this->id        =   (isset($data['id']))        ?    $data['id']        : NULL;
        $this->name      =   (isset($data['name']))      ?    $data['name']      : NULL;
        $this->value     =   (isset($data['value']))     ?    $data['value']     : NULL;
        $this->dead_time =   (isset($data['dead_time'])) ?    $data['dead_time'] : NULL;        
    }   
    

}

