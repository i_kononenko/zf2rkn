<?php
namespace Rkn\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class FilesTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function uploadFiles(Files $files)
    {
        $data = array(
            'date' => time(),
            'xml' => $files->xml,
            'pkcs'  => $files->pkcs,
            'sender'  => $files->sender,
        );        
              
        try{
            $this->tableGateway->insert($data);
        } catch (Exception $ex) {
            echo "Ошибка загруки файлов в БД: ". $ex->GetMessage();
        }
           
    }
    public function getSendFiles() {
        $resultSet = $this->tableGateway->select(array('sender'=>1));
        return $resultSet;                
    }
    
    public function getRequestFiles() {
        $resultSet = $this->tableGateway->select(array('sender'=>2));
        return $resultSet;                
    }
    
    public function setUseFiles($id){
        $id = (int) $id;
        if (isset($id)){
            $this->tableGateway->update(array('used'=>'0'));
            $data = array('used' => '1');           
            $this->tableGateway->update($data, array("id"=>$id));            
        }
        else{            
            throw new Exception("Не выбраны файлы");
        }
    }
    
    public function deleteFiles($id){
        $id = (int) $id;
        if (isset($id)){          
            $this->tableGateway->delete(array("id"=>$id));          
        }
        else{            
            throw new Exception("Не выбраны файлы");
        }
    }

}
