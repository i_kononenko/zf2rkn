<?php
namespace Rkn\Model;

class Files {
    
    public $id;
    public $date;
    public $xml;
    public $pkcs;
    public $sender;
    public $used;
    
    public function exchangeArray($data){
        $this->id =     (isset($data['id']))    ?    $data['id']     : NULL;
        $this->date =   (isset($data['date']))  ?    $data['date']   : NULL;
        $this->xml =    (isset($data['xml']))   ?    $data['xml']    : NULL;
        $this->pkcs =   (isset($data['pkcs']))  ?    $data['pkcs']   : NULL;
        $this->sender = (isset($data['sender']))?    $data['sender'] : NULL;
        $this->used  =  (isset($data['used']))?    $data['used'] : NULL;
    }

}

