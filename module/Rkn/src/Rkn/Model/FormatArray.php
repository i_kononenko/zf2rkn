<?php
namespace Rkn\Model;

class FormatArray {
    protected $fileArray;
    
    private function getUploadFile($files) {
        $this->fileArray = new \ArrayObject();
        $files = $files->getArrayCopy();
        foreach ($files as $key=>$file){
            $fp = fopen($file['tmp_name'], "rb");
            $contents = fread($fp, $file['size']);
            fclose($fp);
            $this->fileArray->offsetSet($key,$contents);
        }
        return $this->fileArray->getArrayCopy();
    }
    
    public function mergeArray($method,$files) {
        $filesArray = $this->getUploadFile($files); 
        foreach($filesArray as $key=>$val){
            $method[$key] = $val;
        }         
        return $method;
    }
}
