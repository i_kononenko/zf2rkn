<?php
namespace Rkn\Model;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DbRkn {

    //Экземпляр класса
    protected $connect = NULL;

    //База
    const DBS = "RKN";
    //Логин
    const LGN = "root";
    //Пароль
    const PWD = "658671";
    //Хост
    const HST = "localhost";

    protected $row;
    protected $par;

    //Конструктор класса
    public function __construct() {
        $ncoding = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8');
        try {
            $this->connect = new \PDO("mysql:dbname=" . self::DBS . ";host=" . self::HST, self::LGN, self::PWD, $ncoding);
        } catch (PDOException $e) {
            echo "Неудалось подключится к базе " . self::DBS . $e->GetMessage();
        }
    }

    private function GetStringRow($row) {
        $Str = "";
        foreach ($row as $k => $v)
            $Str .= empty($Str) ? $v : ", " . $v;
        return $Str;
    }

    private function GetStringRowFromArray($array) {
        $this->row = "";
        $this->par = "";
        foreach ($array[0] as $k => $v) {
            $this->row .= empty($this->row) ? $k . " " : ", " . $k;
            $this->par .= empty($this->par) ? ":" . $k . " " : ", :" . $k;
        }
    }

    public function SelectData($table, $row, $condition) {
        $rowStr = $this->GetStringRow($row);
        $query = $this->connect->prepare("SELECT $rowStr FROM $table $condition");
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_NUM);
    }

    public function InsertData($table, $params) {
        $this->GetStringRowFromArray($params);
        $SQL = "INSERT INTO $table(" . $this->row . ") VALUES(" . $this->par . ")";
        $query = $this->connect->prepare($SQL);
        try {
            $this->connect->beginTransaction();
            foreach ($params as $data) {
                $query->execute($data);
            }
            $flag = $this->connect->commit();
            return $flag;
        } catch (PDOException $e) {
            return $e->GetMessage();
        }
    }

}
