<?php
namespace Rkn\Model;

class Rkn {

    //Служба роскомнадзора
    const WSDL = "http://vigruzki.rkn.gov.ru/services/OperatorRequest/?wsdl";

    //Экземпял класса
    protected $soap;
    //Экземпляр соединения с бд;
    protected $connect;
    //Ответ от сервера
    protected $response = "";
    //Ответ от сервера метода выходные параметры
    public $request = "";
    //Ответ от сервера метода выходные параметры - комментарий
    public $requestComment = "";
    //Параметры запросов;
    protected $params = array();

    //Файл запроса
    const RXML = "/var/RKN/files/request.xml";
    //Электронная подпись файла
    const PKCS = "/var/RKN/files/request.xml.sig";

    // Конструктор
    public function __construct(DbRkn $dbconnect) {
        $this->soap = new \SoapClient(RKN::WSDL);
        $this->connect = $dbconnect;
    }

    //Функция получения временной метки последнего обновления выгрузки из реестра
    public function GetLastDate() {
        empty($this->params);
        $this->params['lastDumpDate'] = '';
        $this->response = $this->soap->GetLastDumpDate();
        return date("l jS \of F Y h:i:s A", $this->response->lastDumpDate / 1000);
    }

    private function base64BinaryEncode($file) {
        $buf = "";
        try {
            $fp = fopen(file, "r");
            while (false !== ($char = fgetc($fp))){
                $buf .= $char;
            }
            fclose($fp);
        } catch (Exception $e) {
            echo "Ошибка чтения файла для декодирования перед отправкой на сервер. Код ошибки" . $e->GetMessage();
        }
        return base64_encode($buf);
    }

    private function base64BinaryDecode($file) {
        return base64_decode(preg_replace('~\s~', '', $file));
    }

    private function GetFileFromDB() {
        $table = "xml_files";
        $row = array("xml", "pkcs");
        //$condition = "WHERE sender = $sender AND date = (SELECT MAX(DATE) FROM $table WHERE sender = $sender)";
        $condition = "WHERE used=1";
        $data = $this->connect->SelectData($table, $row, $condition);
        $file[0] = iconv('windows-1251', "UTF-8//IGNORE", $data[0][0]);
        $file[1] = $data[0][1];
        return $file;
    }

    private function PutFileToDB($file) {
        $table = "xml_files";
        $params = array(array("date" => time(), "xml" => $file, "sender" => 2));
        try {
            $this->connect->InsertData($table, $params);
            return true;
        } catch (Exception $e) {
            return ($e->GetMessage());
        }
    }

    private function getResponse($lastParam) {
        $result[0] = $this->response->result;
        $result[1] = $this->response->result ? $this->response->$lastParam : $this->response->resultComment;
        return $result;
    }

    //Метод направления запроса на получение выгрузки из реестра
    public function SendRequest() {
        $this->params = NULL;
        $this->requestComment = NULL;
        $file = $this->GetFileFromDB();
        $this->params["requestFile"] = $file[0]; // $this->base64BinaryEncode(RKN::RXML);
        $this->params["signatureFile"] = $file[1]; //$this->base64BinaryEncode(RKN::PKCS);	
        $this->response = $this->soap->sendRequest($this->params);
        $this->request = $this->response->code;
        $this->requestComment = $this->response->resultComment;
        return $this->response->result;
    }

    //Метод получения результата обработки запроса - выгрузки из реестра
    public function GetResult($code) {
        $this->params = NULL;
        $this->requestComment = NULL;
        $this->params['code'] = $code;
        $this->response = $this->soap->getResult($this->params);
        if ($this->response->result) {
            $this->PutFileToDB($this->response->registerZipArchive);
        } else
            $this->requestComment = $this->response->resultComment;
        return $this->response->result;
    }

}
