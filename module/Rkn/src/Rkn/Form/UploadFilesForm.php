<?php
namespace Rkn\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class UploadFilesForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('Files');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        
        $this->add(array(            
            'name' => 'MAX_FILE_SIZE',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'value' => '10000000'
            )
        ));

        
        $this->add(array(
            'name' => 'xml',
            'attributes' => array(
                'type'  => 'file',                
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Файл данных организации',
            ),
        )); 
        
	$this->add(array(
            'name' => 'pkcs',
            'attributes' => array(
                'type'  => 'file',                
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Файл электронной подписи',
            ),
        )); 

       	$this->add(array(
            'name' => 'sender',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'value' => '1'
            )
        )); 

        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Загрузить',
            ),
            'options' => array(
                'label' => 'Загрузить',
            ),
        )); 
    }
}
