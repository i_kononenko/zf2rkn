<?php
namespace Rkn\Form;

use Zend\InputFilter\InputFilter;

class UploadFilesFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name'       => 'xml',
            'required'   => false,
        ));
        
        $this->add(array(
            'name'       => 'pkcs',                       
            'required'   => false,
        ));
        
        $this->add(array(
            'name'       => 'sender',                        
            'required'   => false,
        ));        
    }
}
