<?php
namespace Rkn\Controller;

use Rkn\Model\Rkn;
use Rkn\Model\DbRkn;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
 
class HomeController extends AbstractActionController
{    
    protected $soap;
    protected $code;


    public function init($param) {
        if ($this->getRequest()->isXmlHttpRequest()) {
            Zend_Controller_Action_HelperBroker::removeHelper('viewRenderer');
        }
        
    }
    
    public function HomeAction()
    {
	return new ViewModel(array(
            'date' => $this->GetLastDate(),
            'code' => $this->sendRequest()
            )
		);
    }
    
    public function getResultAction() {
        $this->GetLastDate();
        $code = $this->request->getQuery('code');        
        if($this->soap->GetResult($code))
            $res =  json_encode (array(0=>"1",));
        else{
            $com = $this->soap->request;
            $rcom = $this->soap->requestComment;
            $res = json_encode (array(0=>"0",1=>$com,2=>$rcom));
        }
        echo $res;
        exit;
    }
    
    protected function GetLastDate() {
        if(!$this->soap){
        $this->soap = new Rkn(new DbRkn());
        return $this->soap->GetLastDate();                
        }
    }
    
    protected function sendRequest() {
        if($this->soap->SendRequest()){
        return $this->soap->request;
        }
        else{
            $this->redirect()->toRoute('error', array('action' =>  'index','id'=>'1'));
        }
        
    }
}
