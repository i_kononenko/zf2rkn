<?php
namespace Rkn\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class RequestController extends AbstractActionController{

    public function indexAction()
    {
        $tableFiles = $this->getServiceLocator()->get('FilesTable');
	$viewModel  = new ViewModel(array(
            'files' => $tableFiles->getRequestFiles(),
        )); 
	return $viewModel; 
    }
    
    public function downloadAction()
    {
        $tableFiles = $this->getServiceLocator()->get('FilesTable');
	$viewModel  = new ViewModel(array(
            'files' => $tableFiles->getRequestFiles(),
        )); 
	return $viewModel; 
    }
}