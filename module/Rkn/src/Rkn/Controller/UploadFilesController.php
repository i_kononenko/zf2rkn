<?php
namespace Rkn\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Rkn\Model\Files;


class UploadFilesController extends AbstractActionController
{
	
    public function indexAction()
    {
        $form = $this->getServiceLocator()->get('UploadFilesForm');
        $tableFiles = $this->getServiceLocator()->get('FilesTable');
        $fielId = $this->params()->fromRoute('id');
	$viewModel  = new ViewModel(array(
            'form' => $form, 
            'files' => $tableFiles->getSendFiles(),
            'sender' => $fielId,
        )); 
	return $viewModel; 
    }
    
    public function usedAction() {
        $tableFiles = $this->getServiceLocator()->get('FilesTable');
        $tableFiles->setUseFiles($this->params()->fromRoute('id'));
        return $this->redirect()->toRoute('uploadfiles', array('action' =>  'index'));
    }
    
    public function deleteAction() {
        $tableFiles = $this->getServiceLocator()->get('FilesTable');
        $tableFiles->deleteFiles($this->params()->fromRoute('id'));
        return $this->redirect()->toRoute('uploadfiles', array('action' =>  'index'));
    }

    public function processAction()
    {
        if (!$this->request->isPost()) {
            return $this->redirect()->toRoute('uploadfiles', array('action' =>  'index'));
        }
        $formatArray = $this->getServiceLocator()->get('FormatArray');
        
        $post = $this->request->getPost();
        $files = $this->request->getFiles();        
        
        $form = $this->getServiceLocator()->get('UploadFilesForm');        
        $form->setData($post);        
        
        if (!($form->isValid())) {
            $model = new ViewModel(array(
                'error' => true,
                'form'  => $form,
            ));
            $model->setTemplate('rkn/uploadfiles/index');
            return $model;
        }  
        
        $data = $formatArray->mergeArray($form->getData(),$files);             
                
        $this->uploadFiles($data);        
        
        return $this->redirect()->toRoute('uploadfiles', array('action' =>  'confirm'));
    }

    public function confirmAction()
    {
		$viewModel  = new ViewModel(); 
		return $viewModel; 
    }

    protected function uploadFiles(array $data)
    {
		$files = new Files();
		$files->exchangeArray($data);                
                
		$filesTable = $this->getServiceLocator()->get('FilesTable');
		$filesTable->uploadFiles ($files);                
		return true;
    }
}
