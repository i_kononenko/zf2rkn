<?php

namespace Rkn;

use Rkn\Form\UploadFilesForm;
use Rkn\Form\UploadFilesFilter;
use Rkn\Model\FilesTable;
use Rkn\Model\FormatArray;
use Zend\Mvc\ModuleRouteListener;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\MvcEvent;
use Rkn\Model\Files;

class Module {

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function onBootstrap($e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $sharedEventManager = $eventManager->getSharedManager();

        $sharedEventManager->attach(__NAMESPACE__, MvcEvent::EVENT_DISPATCH, function($e) {
            $controller = $e->getTarget();
            $controllerName = $controller->getEvent()->getRouteMatch()->getParam('controller');

            if (!in_array($controllerName, array(
                    'Rkn\Controller\Home',
                    'Rkn\Controller\UploadFiles',
                    'Rkn\Controller\Request',
                    ))) {
                $controller->layout('layout/myaccount');
            }
        });
    }

    public function getServiceConfig() {
        return array(
            'abstract_factories' => array(),
            'aliases' => array(),
            'factories' => array(
                'UploadFilesForm' => function ($sm) {
                    $form = new UploadFilesForm();
                    $form->setInputFilter($sm->get('UploadFilesFilter'));
                    return $form;                                    
                },                
                'FilesTable' =>  function($sm) {
                    $tableGateway = $sm->get('FilesTableGateway');
                    $table = new FilesTable($tableGateway);
                    return $table;
                },
                'FilesTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Files());
                    return new TableGateway('xml_files', $dbAdapter, null, $resultSetPrototype);
                },
                'InfoTable' =>  function($sm) {
                    $tableGateway = $sm->get('InfoTableGateway');
                    $table = new InfoTable($tableGateway);
                    return $table;
                },
                'InfoTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Info());
                    return new TableGateway('info', $dbAdapter, null, $resultSetPrototype);
                },
                'UploadFilesFilter' => function ($sm) {
                    return new UploadFilesFilter();
    		}, 
                'FormatArray' => function ($sm) {
                    return $GetFiles = new FormatArray();                      
    		}, 
                ),
            'invokables' => array(),
            'services' => array(),
            'shared' => array(),
        );
    }

}
