﻿<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Rkn\Controller\Home' => 'Rkn\Controller\HomeController',
            'Rkn\Controller\UploadFiles' => 'Rkn\Controller\UploadFilesController',
            'Rkn\Controller\Request' => 'Rkn\Controller\RequestController',
        ),
    ),
    
    'router' => array(
        'routes' => array(
            'home' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/home[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Rkn\Controller\Home',
                        'action'     => 'home',
                    ),
                ),
            ),
            'uploadfiles' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/uploadfiles[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Rkn\Controller\UploadFiles',
                        'action'     => 'index',
                    ),
                ),
            ),
            'request' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/request[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Rkn\Controller\Request',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
           'home' => __DIR__ . '/../view',
           'uploadfiles' => __DIR__ . '/../view',
           'request' => __DIR__ . '/../view',
        ),
    ),
);
